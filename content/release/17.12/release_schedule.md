+++
date = "2017-10-10"
weight = 100

title = "17.12 Release schedule"

aliases = [
    "/old-wiki/17.12/Release_schedule"
]
+++

The 17.12 release cycle starts in October 2017.

| Milestone                                                                                   | Date                  |
| ------------------------------------------------------------------------------------------- | --------------------- |
| Start of release cycle                                                                      | 2017-10-02            |
| Soft feature freeze: end of feature proposal and review period                              | 2017-11-17            |
| Hard feature freeze: end of feature development for this release                            | 2017-11-30            |
| Release candidate 1 (RC1)/hard code freeze: no new code changes may be made after this date | 2017-12-13 EOD        |
| RC1 testing                                                                                 | 2017-12-14/2017-12-15 |
| 17.12 release                                                                               | 2017-12-18            |

If the release candidate 1 does not successfully pass all required
tests, then the issues will be fixed and a new release candidate will be
re-tested. This would delay the release, which would be reflected on
this page.

## See also

  - Previous [release schedules]( {{< ref "/releases.md" >}} ) and more
    information about the timeline
