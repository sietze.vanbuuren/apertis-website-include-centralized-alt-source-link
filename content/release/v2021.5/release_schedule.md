+++
date = "2022-03-02"
weight = 100

title = "v2021.5 Release schedule"
+++

The v2021.5 release cycle started in January 2022.

| Milestone                                                                                                | Date              |
| -------------------------------------------------------------------------------------------------------- | ----------------- |
| Start of release cycle                                                                                   | 2022-04-01        |
| Soft feature freeze: end of feature proposal and review period                                           | 2022-05-11        |
| Soft code freeze/hard feature freeze: end of feature development for this release, only bugfixes allowed | 2022-05-18        |
| Release candidate 1 (RC1)/hard code freeze: no new code changes may be made after this date              | 2022-05-25        |
| RC testing                                                                                               | 2022-05-26..06-08 |
| v2021.5 release                                                                                          | 2022-06-09        |

If the release candidate 1 does not successfully pass all required
tests, then the issues will be fixed and a new release candidate will be
re-tested. This would delay the release, which would be reflected on
this page.

## See also

  - Previous [release schedules]( {{< ref "/policies/releases.md" >}} ) and
    more information about the timeline
