+++
date = "2022-03-18"
weight = 100

title = "v2022.0 Release Notes"
+++

Apertis is a Debian derivative distribution geared towards the creation
of product-specific images for ARM (both the 32bit ARMv7 and 64-bit
ARMv8 versions using the hardfloat ABI) and Intel x86-64 (64-bit)
systems.

**Apertis v2022.0** is the first **stable** release of the Apertis v2022
stable [release flow]( {{< ref "release-flow.md#apertis-release-flow" >}} ).
Apertis is committed to maintaining the v2022 release stream up to the end
of 2023.

This Apertis release is built on top of Debian Bullseye with several
customisations and the Linux kernel 5.15.x LTS series.

Test results for the v2022.0 release are available in the following
test reports:

  - [APT images](https://lavaphabbridge.apertis.org/report/v2022/20220309.0316/apt)
  - [OSTree images](https://lavaphabbridge.apertis.org/report/v2022/20220309.0316/ostree)
  - [NFS artifacts](https://lavaphabbridge.apertis.org/report/v2022/20220309.0316/nfs)
  - [LXC containers](https://lavaphabbridge.apertis.org/report/v2022/20220309.0316/lxc)

## Release flow

  - 2020 Q4: v2022dev0
  - 2021 Q1: v2022dev1
  - 2021 Q2: v2022dev2
  - 2021 Q3: v2022dev3
  - 2021 Q4: v2022pre
  - **2022 Q1: v2022.0**
  - 2022 Q2: v2022.1
  - 2022 Q3: v2022.2
  - 2022 Q4: v2022.3
  - 2023 Q1: v2022.4
  - 2023 Q2: v2022.5
  - 2023 Q3: v2022.6
  - 2023 Q4: v2022.7

### Release downloads

| [Apertis v2022.0 images](https://images.apertis.org/release/v2022/) | | | | |
| ------------------------------------------------------------------------- |-|-|-|-|
| Intel 64-bit		| [fixedfunction](https://images.apertis.org/release/v2022/v2022.0/amd64/fixedfunction/apertis_ostree_v2022.0-fixedfunction-amd64-uefi_v2022.0.img.gz) | [hmi](https://images.apertis.org/release/v2022/v2022.0/amd64/hmi/apertis_ostree_v2022.0-hmi-amd64-uefi_v2022.0.img.gz) | [base SDK](https://images.apertis.org/release/v2022/v2022.0/amd64/basesdk/apertis_v2022.0-basesdk-amd64-sdk_v2022.0.ova) | [SDK](https://images.apertis.org/release/v2022/v2022.0/amd64/sdk/apertis_v2022.0-sdk-amd64-sdk_v2022.0.ova)
| ARM 32-bit (U-Boot)	| [fixedfunction](https://images.apertis.org/release/v2022/v2022.0/armhf/fixedfunction/apertis_ostree_v2022.0-fixedfunction-armhf-uboot_v2022.0.img.gz) | [hmi](https://images.apertis.org/release/v2022/v2022.0/armhf/hmi/apertis_ostree_v2022.0-hmi-armhf-uboot_v2022.0.img.gz)
| ARM 64-bit (U-Boot)	| [fixedfunction](https://images.apertis.org/release/v2022/v2022.0/arm64/fixedfunction/apertis_ostree_v2022.0-fixedfunction-arm64-uboot_v2022.0.img.gz)
| ARM 64-bit (Raspberry Pi)	| [fixedfunction](https://images.apertis.org/release/v2022/v2022.0/arm64/fixedfunction/apertis_ostree_v2022.0-fixedfunction-arm64-rpi64_v2022.0.img.gz) | [hmi](https://images.apertis.org/release/v2022/v2022.0/arm64/hmi/apertis_ostree_v2022.0-hmi-arm64-rpi64_v2022.0.img.gz)

The Intel `fixedfunction` and `hmi` images are tested on the
[reference hardware (MinnowBoard Turbot Dual-Core)]( {{< ref "/reference_hardware/amd64.md" >}} ),
but they can run on any UEFI-based x86-64 system. The `sdk` image is
[tested under VirtualBox]( {{< ref "/virtualbox.md" >}} ).

#### Apertis v2022.0 repositories

    deb https://repositories.apertis.org/apertis/ v2022 target development sdk


## New features

### Flatpak as the reference application framework

Introduced in [v2021.0]({{< ref "release/v2021.0/releasenotes.md" >}}) as an
alternative to the Canterbury/Ribchester application framework, Flatpak now
has fully replaced it as the reference application framework for Apertis,
as planned in the
[Application framework]({{< ref "application-framework.md" >}}) document.

A [reference `org.apertis` runtime](https://gitlab.apertis.org/infrastructure/apertis-flatpak-runtime)
is also available for application authors to distribute their Apertis-based
applications on top of any Flatpak-supporting system.

Two variants are provided:
- `org.apertis.headless.Platform` and `org.apertis.headless.Sdk`: A basic
  runtime with some common libraries that headless applications may use.
- `org.apertis.hmi.Platform` and `org.apertis.hmi.Sdk`: A larger runtime for
  graphical applications, based on the headless one.

The ["Creating Flatpak Runtimes and Applications]({{< ref "guides/flatpak.md" >}})
guide covers in detail how the runtimes are built, how custom runtimes can be
created and extended and how to build applications using them.

Introduced in [v2022dev2]({{< ref "release/v2022dev2/releasenotes.md" >}})
and [v2022pre]({{< ref "release/v2022dev2/releasenotes.md" >}}).

During this quarter most guides and documents on the Apertis website have
been reviewed and updated to reflect the changes in development and workflows
due to the move to Flatpak.

### AGL-compositor + Maynard as the reference graphical shell

A modern, extensible Wayland compositor and a new reference shell now replace
the Mildenhall HMI, as described in the document about the new
[Application framework]({{< ref "application-framework.md#compositor-libweston" >}}).

{{< figure src="/images/agl-compositor-launcher2-screenshot.png" alt="A screenshot of the Maynard shell and launcher" width="75%" >}}

Shipped as a technology preview in
[v2021.0]({{< ref "release/v2021.0/releasenotes.md" >}}),
Maynard has been the default shell on the HMI images since
[v2022dev2]({{< ref "release/v2022dev2/releasenotes.md" >}}).

Compared to v2022pre, the Flatpak support in AGL-compositor has been improved
to properly handle applications that do not set an application-id based on the
desktop file. With this new support, Flatpak applications can be launched and
handled by the system as any other application.

### PipeWire+WirePlumber as the reference audio manager

From this release the [Pipewire](https://pipewire.org/) multimedia manager and
the [WirePlumber](https://pipewire.pages.freedesktop.org/wireplumber/) policy
manager replace PulseAudio as the default audio manager shipped on the
reference images, as described in the
[audio management]({{< ref "audio-management.md" >}}) concept document.

PipeWire grants more efficiency and flexibility compared to PulseAudio, with
better integration in container-based application frameworks like Flatpak,
while WirePlumber enable developers to define complex policies with ease.

Thanks to `pipewire-pulse` applications using the PulseAudio protocol will
automatically and transparently be able to interact with PipeWire with no
changes on the client side.

Introduced in [v2022dev2]({{< ref "release/v2022dev2/releasenotes.md" >}}).

### Modernization of GPL-3-free replacements

One of the main source of deviation from Debian in Apertis is due to the
different [licensing expectations]({{< ref "license-expectations.md" >}})
regarding GPL-3 components and the Apertis v2022.0 release ships with a massive
modernization of the approach used to meet such expectations.

Compared to Debian, Apertis v2022.0 avoids the GPL-3 terms in its target
images by:

* adopting OpenSSL rather than GnuTLS where the GPL-2 terms are not suitable,
  as described in details in the
  [document about licensing compliance for the TLS stack]({{< ref "tls-stack.md" >}})
  (introduced in [v2022dev2]({{< ref "release/v2022dev2/releasenotes.md" >}}));
* replacing GNU Coreutils with the Rust-based coreutils implementation
  as covered in [the dedicated document]({{< ref "coreutils-replacement.md" >}})
  (introduced in [v2022dev3]({{< ref "release/v2022dev3/releasenotes.md" >}}));
* using the Rust-based Sequoia implementation of OpenPGP to
  [avoid GnuPG]({{< ref "gnupg-replacement.md" >}})
  (introduced in [v2022dev3]({{< ref "release/v2022dev3/releasenotes.md" >}}))

The GPL-3 GNU Coreutils and GnuPG remain available in the `development`
component of the packaging archive and are still used during package builds
where the GPL-3 licensing is not a concern, to maximise compatibility.

Compared to v2022pre, `rust-coreutils` has been updated to the latest upstream
version to provide better compatibility with GNU version. The package now also
ships a multi-call binary to minimize the storage overhead due to the
statically linked nature of Rust executables.

The [GPL-3 Deltas Assessment]({{< ref "gpl3_free_deltas.md" >}}) document
captures the remaining issues that will be addressed in the v2023 cycle.

### WPA3 support in ConnMan

Apertis now ships support for WPA3/SAE in ConnMan. By basing the work on top of the
[patches from](https://review.tizen.org/git/?p=platform/upstream/connman.git;a=commitdiff;h=a48fa9fdffe415e9a6f703776b5db795e242ac23)
[the Tizen project](https://review.tizen.org/git/?p=platform/upstream/connman.git;a=commit;h=d04bfa0350781ebfb8cbb2e64fabdfb2f36cd302)
the Apertis team landed the
[implementation of WPA3-SAE authentication `wpa_supplicant`](https://git.kernel.org/pub/scm/network/connman/connman.git/commit/?id=a08873088de149e6aea771d46946e96fd35ab319)
and
[WPA3-Personal transition mode](https://git.kernel.org/pub/scm/network/connman/connman.git/commit/?id=9a54df53361a12458532dfc06156a2b2e468ea0d)
to upstream ConnMan and made it available in the Apertis `connman` package.

Introduced in [v2022dev2]({{< ref "release/v2022dev2/releasenotes.md" >}})
and [v2022dev3]({{< ref "release/v2022dev2/releasenotes.md" >}})

### Licensing reports

As part of the image building pipeline Apertis now also produces detailed
reports listing the licenses covering the binaries shipped on each image.

The extracted information is stored in a per-image JSON file which documents
the licenses that affect each binary program or library shipped in the image
itself and that can be easily used to generate human-readable reports or to
add automated checks to ensure compliance to licensing policies.

Introduced in [v2022dev1]({{< ref "release/v2022dev1/releasenotes.md" >}}).

### Fixed Function and HMI images to replace minimal and target

Based on the [Apertis Platform Technical Vision]({{< ref "overview.md" >}}) the
set of reference images is now composed of:
- Fixed Function
- HMI
- BaseSDK
- SDK

This gives a clearer purpose to each image type, providing a better guide to
developers looking at the reference images for their product recipes.

The Fixed Function image is meant to represent images that focus on a single,
specific purpose and are built monolithically, with all the components
subject to the same lifecycle. The reference use-case for Fixed Function image
has been selected to be as simple as possible while still being meaningful,
and the choice fell on serving a static web page over HTTP.
The narrow goal of the Fixed Function gives the opportunity to further
optimize it compared to the fuzzy, broad definition of the `minimal` image
in previous releases.

The HMI images focus instead on more elaborate and featureful use-cases: they
boot up in a graphical UI provided by the
[AGL compositor framework]({{< ref "application-framework.md#compositor-libweston" >}})
with the Maynard shell and use the
[Flatpak application framework]({{< ref "application-framework.md#application-runtime-flatpak" >}})
to handle dynamically evolving workloads with life cycles decoupled from the
one for the base operating system.

Introduced in [v2022dev3]({{< ref "release/v2022dev2/releasenotes.md" >}}).

### Improved Raspberry Pi support

Users of Raspberry Pi will find a better support in Apertis including:

- Support for RPi 400 model.
- Graphical interface now booting without input devices attached.
- Fixed USB support.
- Documentation to bring up [Raspberry Pi 4]({{< ref rpi4_setup.md  >}}).

Introduced in [v2022dev3]({{< ref "release/v2022dev2/releasenotes.md" >}}).

### iMX8MN Variscite Symphony and BSH SMM S2 PRO board support

Designed by BSH, the [SystemMaster S2 Pro]({{< ref "imx8mn_bsh_smm_s2pro_setup.md"  >}}) is an add-on board which provides
input and output interfaces to a dedicated carrier board. Apertis users can now
run a headless system using the `fixedfunction` image, which includes hardware
support for wifi, bluetooth and audio.

Support has also been added for the iMX8MN Variscite Symphony board.

Check the setup notes for more info:
* [SystemMaster S2 Pro]({{< ref imx8mn_bsh_smm_s2pro_setup.md  >}})
* [iMX8MN Variscite Symphony]({{< ref imx8mn_var_symphony_setup.md >}})

Introduced in [v2022dev3]({{< ref "release/v2022dev2/releasenotes.md" >}})
and [v2022pre]({{< ref "release/v2022dev2/releasenotes.md" >}}).

### OVA VirtualBox SDK appliance

To make the initial setup easier for users of the Apertis SDK with VirtualBox,
a full OVA appliance file is provided. Developers can now create their Apertis
virtual machine in Virtualbox with just a double click instead of having to
manually set all the right parameters before importing the actual disk image.

Introduced in [v2022dev2]({{< ref "release/v2022dev2/releasenotes.md" >}}).

### Apertis Update Manager improvements

During the release several improvements have been made to make Apertis Update
Manager have similar behavior on boards that uses U-boot and UEFI.
In this context now the boot limit in UEFI board
[has been updated](https://gitlab.apertis.org/pkg/apertis-update-manager/-/merge_requests/85)
to match the U-boot's default.

In addition to this, the ref-binding selection on successful upgrades has been fixed to
[correctly upgrade from development to stable releases](https://gitlab.apertis.org/pkg/apertis-update-manager/-/merge_requests/87).

## Build and integration

### Improve LAVA test development

Apertis uses LAVA tests to ensure the quality of its releases by running
automated tests on its images. These tests should run in different boards with
different architectures with the different types of images. In order to handle
this in an efficient way, the concept of LAVA profile was improved to allow
developers to choose the tests that need to be run on the different
scenarios in a
[simpler and more consistent way](https://gitlab.apertis.org/tests/apertis-test-cases/-/merge_requests/407).

## Deprecations and ABI/API breaks

### Regressions

No known regressions.

### Deprecations

No known deprecations

### Breaks

#### GPL-3 replacements revamp on target images (HMI and FixedFunction)

On target images (HMI and FixedFunction) the GPL-3 GNU Coreutils and GnuPG
tools are no longer available.

The `rust-coreutils` package installed on the images is meant to be a fully
compatible replacement for GNU Coreutils, but some differences still persist.

GNU Coreutils and GnuPG remain available in the `development` packaging
repository and are used by default at build time on OBS and on the SDK images.

Some libraries (`glib-networking` in particular) have been switched to their
OpenSSL backends to avoid the LGPL-3 terms of the GnuTLS stack when linked
from non-GPL-2 programs.

Introduced in [v2022dev2]({{< ref "release/v2022dev2/releasenotes.md" >}})
and [v2022dev3]({{< ref "release/v2022dev3/releasenotes.md" >}}).

#### Mildenhall HMI no longer available

The Maynard shell on top of the modular AGL compositor replaces the Mildenhall
compositor and launcher. as described in the document about the new
[Application framework]({{< ref "application-framework.md#compositor-libweston" >}}).

Upstream toolkits like GTK are recommended to replace any usage of the
Mildenhall UI toolkit.

The existing Mildenhall-based applications can still be used through Flatpak,
using the v2021 runtime.

Introduced in [v2022dev2]({{< ref "release/v2022dev2/releasenotes.md" >}}).

#### Canterbury application framework no longer available

Flatpak is now the reference application framework, replacing Canterbury/Ribchester.

Introduced in [v2022dev2]({{< ref "release/v2022dev2/releasenotes.md" >}}).

#### PipeWire used by default instead of Pulseaudio

Audio management is now handled with PipeWire and WirePlumber on all images.

Pulseaudio will remain available in the package repositories, even if not installed by defrault.

Introduced in [v2022dev2]({{< ref "release/v2022dev2/releasenotes.md" >}}).

## Infrastructure

### Apertis Docker images

The Apertis Docker images provide a unified and easily reproducible build
environment for developers and services.

As of today, this includes the
[`apertis-base`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022-base),
[`apertis-image-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022-image-builder),
[`apertis-package-source-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022-package-source-builder),
[`apertis-flatdeb-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022-flatdeb-builder),
[`apertis-documentation-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022-documentation-builder),
and [`apertis-testcases-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022-testcases-builder),
Docker images.

### Apertis infrastructure tools

The [Apertis v2022 infrastructure repository](https://build.collabora.co.uk/project/users/apertis:infrastructure:v2022)
provides packages for the required versions of `ostree-push` and
`ostree` for Debian Buster:

    deb https://repositories.apertis.org/infrastructure-v2022/ buster infrastructure

### Images

Image daily builds, as well as release builds can be found at <https://images.apertis.org/>

### Infrastructure overview

The [Apertis Image Recipes](https://gitlab.apertis.org/infrastructure/apertis-image-recipes/)
provides an overview of the image building process and the involved
services.

## Known issues

### High (15)
 - [T7879](https://phabricator.apertis.org/T7879)	sdk-debos-image-building: test failed
 - [T8456](https://phabricator.apertis.org/T8456)	aum-api: test failed
 - [T8470](https://phabricator.apertis.org/T8470)	secure-boot-imx6 test failed
 - [T8530](https://phabricator.apertis.org/T8530)	evolution-sync-bluetooth test not available in v2022 and v2023dev1 testcase page
 - [T8545](https://phabricator.apertis.org/T8545)	BOM file generation pick default license
 - [T8547](https://phabricator.apertis.org/T8547)	sanity-check: test failed
 - [T8603](https://phabricator.apertis.org/T8603)	AUM fails to detect rollback on Lava for RPi64 board
 - [T8604](https://phabricator.apertis.org/T8604)	AUM tests fails on Lava for RPi64 board
 - [T8610](https://phabricator.apertis.org/T8610)	Architecture dependent license files at generated packages are created at architecture independent locations
 - [T8615](https://phabricator.apertis.org/T8615)	aum-ota-api: test failed
 - [T8625](https://phabricator.apertis.org/T8625)	gstreamer1-0-decode: test failed
 - [T8654](https://phabricator.apertis.org/T8654)	Texlive-extra package trigger again and again on OBS.
 - [T8660](https://phabricator.apertis.org/T8660)	Random FS issues on OSTree images
 - [T8662](https://phabricator.apertis.org/T8662)	aum-rollback-blacklist: test failed
 - [T8669](https://phabricator.apertis.org/T8669)	aum-ota-out-of-space: test failed

### Normal (55)
 - [T2896](https://phabricator.apertis.org/T2896)	Crash when initialising egl on ARM target
 - [T3233](https://phabricator.apertis.org/T3233)	Ribchester: deadlock when calling RemoveApp() right after RollBack()
 - [T3321](https://phabricator.apertis.org/T3321)	libgles2-vivante-dev is not installable
 - [T3920](https://phabricator.apertis.org/T3920)	arm-linux-gnueabihf-pkg-config does not work with sysroots installed by `ade`
 - [T4092](https://phabricator.apertis.org/T4092)	Containers fail to load on Gen4 host
 - [T4307](https://phabricator.apertis.org/T4307)	ribchester-core causes apparmor denies on non-btrfs minimal image
 - [T4422](https://phabricator.apertis.org/T4422)	do-branching fails at a late stage cloning OBS binary repos
 - [T4693](https://phabricator.apertis.org/T4693)	Not able to create namespace for AppArmor container on the internal mx6qsabrelite images with proprietary kernel
 - [T5487](https://phabricator.apertis.org/T5487)	Wi-Fi search button is missing in wifi application
 - [T5748](https://phabricator.apertis.org/T5748)	System users are shipped in /usr/etc/passwd instead of /lib/passwd
 - [T5896](https://phabricator.apertis.org/T5896)	sdk-dbus-tools-bustle testcase is failing
 - [T5897](https://phabricator.apertis.org/T5897)	apparmor-ofono test fails
 - [T5900](https://phabricator.apertis.org/T5900)	evolution-sync-bluetooth test fails
 - [T5931](https://phabricator.apertis.org/T5931)	connman-usb-tethering test fails
 - [T6024](https://phabricator.apertis.org/T6024)	sdk-dbus-tools-d-feet: folks-inspect: command not found
 - [T6077](https://phabricator.apertis.org/T6077)	youtube Videos are not playing on upstream webkit2GTK
 - [T6078](https://phabricator.apertis.org/T6078)	Page scroll is lagging in Minibrowser on upstream webkit2GTK
 - [T6111](https://phabricator.apertis.org/T6111)	traprain: 7_traprain test failed
 - [T6292](https://phabricator.apertis.org/T6292)	gettext-i18n: test failed
 - [T6349](https://phabricator.apertis.org/T6349)	sdk-code-analysis-tools-splint: 3_sdk-code-analysis-tools-splint test failed
 - [T6366](https://phabricator.apertis.org/T6366)	sdk-cross-compilation: 10_sdk-cross-compilation test failed
 - [T6768](https://phabricator.apertis.org/T6768)	Fix the kernel command line generation in OSTRee for FIT image
 - [T6773](https://phabricator.apertis.org/T6773)	HAB testing: the unsigned image may pass validation in several circumstances
 - [T6795](https://phabricator.apertis.org/T6795)	SabreLite failing to boot due to failing "to start udev Coldplug all Devices"
 - [T6806](https://phabricator.apertis.org/T6806)	HAB on SabreLite in open state accepts any signed kernel regardless of the signing key
 - [T6961](https://phabricator.apertis.org/T6961)	audio-backhandling feature fails
 - [T7000](https://phabricator.apertis.org/T7000)	DNS resolution does not work in Debos on some setups
 - [T7016](https://phabricator.apertis.org/T7016)	network proxy for browser application is not resolving on mildenhall-compositor
 - [T7512](https://phabricator.apertis.org/T7512)	debos sometimes fails to mount things
 - [T7721](https://phabricator.apertis.org/T7721)	Fakemachine in debos immediately powers off and hangs in v2021 and v2022dev1 when using UML on the runners
 - [T7785](https://phabricator.apertis.org/T7785)	DNS over TLS does not work on systemd-resolve
 - [T7817](https://phabricator.apertis.org/T7817)	rhosydd: test failed
 - [T7819](https://phabricator.apertis.org/T7819)	newport: test failed
 - [T7852](https://phabricator.apertis.org/T7852)	Investigate test failure TestGetSourceMount
 - [T7859](https://phabricator.apertis.org/T7859)	spymemcached: Investigate failing test due to hostname mismatch
 - [T7872](https://phabricator.apertis.org/T7872)	Error building package ruby-redis on OBS
 - [T7923](https://phabricator.apertis.org/T7923)	Gitlab pipeline OBS job reported "success" but OBS build was "unresolvable"
 - [T7945](https://phabricator.apertis.org/T7945)	evolution-sync-bluetooth test fails
 - [T8175](https://phabricator.apertis.org/T8175)	License scan fails on package texlive-extra
 - [T8194](https://phabricator.apertis.org/T8194)	ci-license-scan prints final error paragraph in the middle of scan-copyrights output
 - [T8281](https://phabricator.apertis.org/T8281)	dashboard: gnome-settings-daemon-data incorrectly flagged as missing the git repository
 - [T8435](https://phabricator.apertis.org/T8435)	"Add 'reserved-memory' node failed: FDT_ERR_EXISTS" log during boot
 - [T8500](https://phabricator.apertis.org/T8500)	canterbury: test failed
 - [T8504](https://phabricator.apertis.org/T8504)	LAVA/Phab bridge timeouts
 - [T8516](https://phabricator.apertis.org/T8516)	apparmor-pipewire: test failed
 - [T8524](https://phabricator.apertis.org/T8524)	grilo: test failed
 - [T8572](https://phabricator.apertis.org/T8572)	Missing warning on coreutils overwrite
 - [T8613](https://phabricator.apertis.org/T8613)	apparmor-functional-demo: test failed
 - [T8622](https://phabricator.apertis.org/T8622)	Manual testcase results should not have any hyperlink in the LavaPhabridge report page
 - [T8629](https://phabricator.apertis.org/T8629)	frome: test failed
 - [T8634](https://phabricator.apertis.org/T8634)	Failed to start Access poi…server : logs seen in v2023dev1 Amd64 boot logs
 - [T8667](https://phabricator.apertis.org/T8667)	cgroups-resource-control: test failed
 - [T8668](https://phabricator.apertis.org/T8668)	Test apparmor-functional does not work properly on OSTree images
