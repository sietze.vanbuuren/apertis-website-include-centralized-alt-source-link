+++
date = "2021-07-13"
lastmod = "2021-08-04"
weight = 100
toc = true

title = "i.MX8MN Variscite Symphony board"
+++

Variscite [Symphony-Board](https://www.variscite.com/product/single-board-computers/symphony-board/)
Single Board Computer (SBC) in conjunction with [VAR-SOM-MX8M-NANO](https://www.variscite.com/product/system-on-module-som/cortex-a53-krait/var-som-mx8m-nano-nxp-i-mx-8m-nano/)
System on Module (SoM) based on NXP’s [i.MX 8M Nano](https://www.nxp.com/products/processors-and-microcontrollers/arm-processors/i-mx-applications-processors/i-mx-8-processors/i-mx-8m-nano-family-arm-cortex-a53-cortex-m7:i.MX8MNANO).

Variscite provides two [evaluation kits](https://www.variscite.com/product/evaluation-kits/var-som-mx8m-nano-evaluation-kits/)
variants: Starter Kit and Development Kit, which comes with Display and Touch
Panel.

![iMX8MN Variscite Development Kit](/images/imx8mn_symphony_kit.jpg)

This is the setup recommended for developers using the Variscite Symphony board.
See [LAVA boards setup](https://lava.pages.collabora.com/docs/boards/)
for setting up devices for use in LAVA.

# Required items

You should have at least:

- 1 iMX8MN Variscite Development Kit.
- 1 Power supply/adaptor 12V/3A DC, center positive.
- 1 USB type A to micro B cable.
- 1 MicroSD card. At least 4GB for fixedfunction image.

# Board setup

Apertis provides its own build of U-Boot for the iMX8MN board that it expects to
be flashed into the eMMC. This is preferred over storing U-Boot on the SD card
as it allows a common Apertis image to be programmed onto the SD card.

- Download and program the iMX8MN Variscite Symphony installer (called
  `uboot-<version>-installer-imx8mn_var_som.img.gz` to the SD card. Images can
  be found under the `installer` directory of the relevant release on the
  [Apertis image download](https://images.apertis.org/) site.
- Insert the SD card into the SD card slot (J28).
- Plug the USB type A to micro B cable into the USB Debug Connector (J29).
  Use serial port settings 115200 8N1 to access the debug console.
- Ensure the Boot Select Switch (SW3) is set to SD (micro SD card) instead of
  INT (eMMC/NAND).

![iMX8MN Variscite Boot Switch](/images/imx8mn_symphony_boot.jpg)

- Connect the power supply/adaptor to the DC Power Jack (J24). Power up the
  board by switching on the Power ON Switch (SW7), which is placed right next to
  the DC Jack.
- Once the Variscite Symphony board has finished programming, the following
  message will be displayed:

<!-- end list -->

    +-----------------------------------------------------------------+
    |                  U-Boot installation complete                   |
    |                                                                 |
    | Please remove the SD Card and power cycle the board to continue |
    +-----------------------------------------------------------------+

- Power off the board and remove the SD card.
- [Download]({{< ref "download.md" >}}) and program the required image onto the
  SD card.
- Set the Boot Select Switch (SW3) to INT (eMMC/NAND) instead of SD (micro SD
  card).
- Insert the SD card and power up the Variscite Symphony board.

# Pending hardware support for this board:

This board is in early stage development. Support might not be complete for the
following hardware components:

* Display
* Touchscreen
