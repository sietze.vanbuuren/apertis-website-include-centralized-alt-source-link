+++
date = "2022-05-27"
weight = 100

title = "Intel Atom x6425RE UP Squared 6000 board"
+++

[UP Squared 6000](https://up-board.org/up-squared-6000/) series board based on
the latest [Intel Atom x6425RE](https://ark.intel.com/content/www/us/en/ark/products/207899/intel-atom-x6425re-processor-1-5m-cache-1-90-ghz.html)
processors.

![UP Squared 6000 AT 08/64 board](/images/upsquared6000_board.jpg)

# Required items

You should have at least:

- 1 UP Squared 6000 board. Available on
  [up-shop](https://up-shop.org/ups6000series.html).
- 1 Power Supply 12V/6A DC. This is designated for UP Squared 6000, check
  [up-shop](https://up-shop.org/12v/6a%2872w%29_psu.html).
- 1 USB mass-storage device. At least 4GB for fixedfunction image and 15GB for
  HMI image.

In order to connect to the debug console UART, you also need:

- 10-pin wafer box connector cable. Available from the
  [up-shop](https://up-shop.org/usb-2-0-pin-header-cable.html).
- 3.3V Serial cable (for example, an FTDI cable).
- Depending on the type of serial cable: jumper wires.

## Optional items

You may also need a:

- Intel AX210 wifi module, for wireless/bluetooth connectivity.
- Lilliput touchscreen, required if using the HMI image.

# Board setup

1. [Download]({{< ref "download.md" >}}) the Intel 64-bit (amd64) HMI or
   fixedfunction image and `.bmap` file.
2. Flash the image to a USB mass-storage device (e.g. `/dev/sda`) using command:
```
sudo bmaptool copy path/to/image.img.gz /dev/sda
```
3. Plug the USB mass-storage device in the board.
4. Plug in the UART 10-pin header cable on the USB 2.0 / UART wafer. Connect to
   the serial console running any terminal emulator like:
```
picocom /dev/ttyUSB0 -b 115200
```
5. Plug in the power supply and wait for it to boot.

## Serial console

The 10-pin header adapter from the [up-shop](https://up-shop.org/usb-2-0-pin-header-cable.html)
brings three UART connectors out of the USB 2.0 / UART wafer.

![UP Squared 6000 AT 08/64 board serial](/images/upsquared6000_board_serial.jpg)

Check the official board documentation for
[serial console access](https://github.com/up-board/up-community/wiki/Serial-Console#using-the-provided-adapter).

<table class="wikitable">
<tr>
<th><tt>CN7</tt> pin
</th>
<th>Description
</th>
<th>Group
</th>
<th>Adapter connector
</th></tr>
<tr>
<td>1    </td>
<td> <tt>USB_VCC</tt> </td>
<td rowspan="4"> <tt>USB 1</tt> </td>
<td rowspan="4"> USB 2.0 plug
</td></tr>
<tr>
<td>2    </td>
<td> <tt>USB_HSIC_P3_D-</tt>
</td></tr>
<tr>
<td>3    </td>
<td> <tt>USB_HSIC_P3_D+</tt>
</td></tr>
<tr>
<td>4    </td>
<td> <tt>GND</tt>
</td></tr>
<tr>
<td>5    </td>
<td> <tt>USB_VCC</tt> </td>
<td rowspan="4"> <tt>USB 1</tt> </td>
<td rowspan="4"> USB 2.0 plug
</td></tr>
<tr>
<td>6    </td>
<td> <tt>USB_HSIC_P3_D-</tt>
</td></tr>
<tr>
<td>7    </td>
<td> <tt>USB_HSIC_P3_D+</tt>
</td></tr>
<tr>
<td>8    </td>
<td> <tt>GND</tt>
</td></tr>
<tr>
<td>9    </td>
<td> <tt>UART0_RXD</tt> </td>
<td rowspan="3"> <tt>UART0</tt> </td>
<td> 3-pin: pin 2 (red)
</td></tr>
<tr>
<td>10   </td>
<td> <tt>UART0_TXD</tt>                               </td>
<td> 3-pin: pin 1 (white, with ▲ mark)
</td></tr>
<tr>
<td>4, 8 </td>
<td> <tt>GND</tt>                                     </td>
<td> 3-pin: pin 3 (black)
</td></tr></table>

## Touchscreen

If you have a Lilliput touchscreen, note that because it combines HDMI video and USB for the touchscreen into one connector, it needs to be used with the special cable provided:

* Connect the two-tailed HDMI connector to the UP Board (through the type A to type D adaptor).
* Connect the USB lead to the UP Board.
* Connect the one-tailed HDMI connector to the Lilliput.
* Power up the Lilliput.

As in the diagram:

<table class="wikitable">
<tr>
<td>
<pre><code>
          | HD ---------------- &quot;HDMI&quot; |
UP-BOARD  | MI --\                     | SCREEN
          |      |                     |
          | USB -/                     |
</code></pre>
</td>
</tr>
</tbody>
</table>

## References

- UP board serial console wiki documentation:
  https://github.com/up-board/up-community/wiki/Serial-Console
- UP Squared 6000 board pinout wiki documentation:
  https://github.com/up-board/up-community/wiki/Pinout_UP6000
