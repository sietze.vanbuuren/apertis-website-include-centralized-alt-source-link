+++
date = "2019-09-30"
weight = 100

title = "i.MX6Q Sabrelite Setup in LAVA"

aliases = [
    "/old-wiki/Reference_Hardware/imx6q_sabrelite_setup_lava"
]
+++

These are the instructions to prepare a device for use as a DUT in the
Apertis LAVA automated test laboratory. See
[Reference_Hardware/imx6q_sabrelite_setup]( {{< ref "/reference_hardware/imx6q_sabrelite_setup.md" >}} )
for the recommended setup for developers' devices.

# Setting up an imx6q-sabrelite device for LAVA

When running the SABRE Lite as a DUT in LAVA, we install U-Boot in the
SPI NOR Flash memory. This allows the software under test to be loaded
as an image, including it's own bootloader, on the SD card.

LAVA will typically make the device boot twice: first with the
persistent bootloader in the SPI memory to download and write an OS
image on the SD card, then reboot automatically a second time from the
SD card with the freshly flashed image.

  - Download the following U-Boot image installer
  [uboot-v2023dev2-installer-mx6qsabrelite-20220511.0019.img.gz](https://images.apertis.org/lava/boot/mx6qsabrelite/uboot-v2023dev2-installer-mx6qsabrelite-20220511.0019.img.gz). Note that other related files to this image, e.g. bmap file, can be downloaded as well from
  [here](https://images.apertis.org/lava/boot/mx6qsabrelite/).

  - Follow the instructions [developer
    instructions]( {{< ref "/reference_hardware/imx6q_sabrelite_setup.md" >}} )
    to get U-Boot installed on the SABRE Lite.

<!-- end list -->

  - Disable autoboot with the following commands:

<!-- end list -->

    => setenv bootdelay -1
    => saveenv

# Rebooting from the SD card

Once U-Boot has been loaded from the SPI flash, it is possible to set a
hardware register flag and reboot the board on the SD card with the
commands below (LAVA job definition snippet):

` - boot:`
`     namespace: system`
`     method: u-boot`
`     commands: ['mw.l 0x020d8040 0x3040 && mw.l 0x020d8044 0x10000000', 'reset']`

This flag is not persistent so these commands need to be run every time
the board needs to boot from the SD card.
