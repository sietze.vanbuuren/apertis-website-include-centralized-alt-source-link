+++
date = "2021-04-27"
weight = 100

title = "Robot Framework"
+++

[Robot Framework](https://robotframework.org/) is a generic test automation framework 
for acceptance testing and acceptance test-driven development (ATDD). It has easy-to-use 
tabular test data syntax and it utilizes the keyword-driven testing approach. 
Its testing capabilities can be extended by test libraries implemented with either Python 
or Java, and users can create new higher-level keywords from existing ones using the same 
syntax that is used for creating test cases. It is open source software released 
under [Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0), and most of the 
libraries and tools in the ecosystem are open source. The framework was initially developed 
at Nokia Networks and it is nowadays sponsored by
[Robot Framework Foundation](https://robotframework.org/foundation).

# Standard Libraries and Built-In Tools

These test libraries are distributed with Robot Framework:
- BuiltIn
- Collections
- Date Time
- Dialogs
- Operating System
- Process
- Remote
- Screenshot
- String
- Telnet
- XML

It has the following supporting tools built-in:
- Rebot
- Libdoc
- Testdoc
- Tidy

# External SSHLibrary

SSHLibrary provides the ability to perform tests over SSH and SFTP. It is operating system independent 
and supports Python 2.7 as well as Python 3.4 or newer. This library is integrated 
with robotframework and is used by Apertis tests to enable testing of target devices 
without needing to install the Robot Framework directly on them.

# Installation

The Robot Framework has been packaged in Apertis and can be installed on the SDK with the following commands:

    $ sudo apt update
    $ sudo apt install python3-robotframework python3-robotframework-sshlibrary

# Verifying Installation

After a successful installation, we should be able to execute the Robot Framework with the `--version` option and get both Robot Framework and interpreter versions as a result:
    $ robot --version
    Robot Framework 4.0.1 (Python 3.7.3 on linux)
    $ rebot --version
    Rebot 4.0.1 (Python 3.7.3 on linux)


