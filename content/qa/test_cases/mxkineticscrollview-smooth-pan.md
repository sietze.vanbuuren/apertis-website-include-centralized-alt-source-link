+++
date = "2017-10-23"
weight = 100

title = "mxkineticscrollview-smooth-pan"

aliases = [
    "/old-wiki/QA/Test_Cases/mxkineticscrollview-smooth-pan"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
