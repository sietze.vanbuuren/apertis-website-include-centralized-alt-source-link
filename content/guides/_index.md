+++
date = "2020-10-01"
weight = 100

title = "Guides"

aliases = [
    "/old-developer/latest/programming-guide-index.html",
    "/old-developer/v2019/programming-guide-index.html",
    "/old-developer/v2020/programming-guide-index.html",
    "/old-developer/v2021pre/programming-guide-index.html",
    "/old-developer/v2022dev0/programming-guide-index.html",
    "/old-wiki/Guidelines"
]
+++

The guides provide detailed guidance on performing specific tasks or utilize specific features provided by Apertis.

For higher level descriptions of the technology employed in Apertis, please see the [architecture documents]({{< ref "architecture/_index.md" >}}).
